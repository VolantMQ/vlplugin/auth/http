package main

import (
	"context"
	"net/http"
	"os"
	"regexp"
	"testing"

	"github.com/VolantMQ/vlapi/vlauth"
	"github.com/emicklei/go-restful"
	"github.com/stretchr/testify/require"
)

var (
	regexpAuthBearer     = regexp.MustCompile(`^(Bearer )([A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*)$`)
	regexpPermissionType = regexp.MustCompile(`^write|read$`)

	allowedUserClient = map[string]string{
		"testuser": "testclient",
	}

	allowedUserPassword = map[string]string{
		"testuser": "testpassword",
	}

	allowedRead = map[string]string{
		"testclient": "^/topic/v1/.+$",
	}

	allowedWrite = map[string]string{
		"testclient": "^/topic/v2/.+$",
	}
)

const (
	verySecretToken = string("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
)

func authn(request *restful.Request, response *restful.Response) {
	resp := vlauth.StatusDeny

	defer func() {
		response.WriteHeader(http.StatusOK)
		_, _ = response.Write([]byte(resp.Desc()))
	}()

	var err error
	var clientID string
	var user string
	var pass string

	if clientID, err = request.BodyParameter(paramClientID); err != nil || clientID == "" {
		return
	}

	if user, err = request.BodyParameter(paramUser); err != nil {
		return
	}

	if pass, err = request.BodyParameter(paramPassword); err != nil {
		return
	}

	if id, ok := allowedUserClient[user]; !ok || id != clientID {
		return
	}

	if ps, ok := allowedUserPassword[user]; !ok || ps != pass {
		return
	}

	resp = vlauth.StatusAllow
}

func authz(request *restful.Request, response *restful.Response) {
	resp := vlauth.StatusDeny

	defer func() {
		response.WriteHeader(http.StatusOK)
		_, _ = response.Write([]byte(resp.Desc()))
	}()

	var err error
	var clientID string
	var user string
	var name string
	var permission string

	if clientID, err = request.BodyParameter(paramClientID); err != nil || clientID == "" {
		return
	}

	if user, err = request.BodyParameter(paramUser); err != nil {
		return
	}

	if name, err = request.BodyParameter(paramName); err != nil || name == "" {
		return
	}

	if permission, err = request.BodyParameter(paramPermission); err != nil || !regexpPermissionType.Match([]byte(permission)) {
		return
	}

	if id, ok := allowedUserClient[user]; !ok || id != clientID {
		return
	}

	var perm string

	if permission == "write" {
		p, ok := allowedWrite[clientID]
		if !ok {
			return
		}
		perm = p
	} else {
		p, ok := allowedRead[clientID]
		if !ok {
			return
		}
		perm = p
	}

	var rgxp *regexp.Regexp

	if rgxp, err = regexp.Compile(perm); err != nil {
		return
	}

	if !rgxp.Match([]byte(name)) {
		return
	}

	resp = vlauth.StatusAllow
}

func apiAuth(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	writeResp := false

	defer func() {
		if writeResp {
			_ = resp.WriteErrorString(401, "401: Not Authorized")
		}
	}()

	authHeader := req.HeaderParameter("Authorization")

	if !regexpAuthBearer.MatchString(authHeader) {
		writeResp = true
		return
	}

	match := regexpAuthBearer.FindAllStringSubmatch(authHeader, -1)

	if match[0][2] != verySecretToken {
		writeResp = true
		return
	}

	chain.ProcessFilter(req, resp)
}

func TestMain(m *testing.M) {
	ws := new(restful.WebService)

	ws.Path("/mqttauth").
		Doc("MQTT auth")

	ws.Route(ws.POST("/user").
		Produces("text/plain").
		Consumes("application/x-www-form-urlencoded").
		To(authn).
		Param(ws.FormParameter(paramClientID, "ClientID").DataType("string")).
		Param(ws.FormParameter(paramUser, "User name").DataType("string")).
		Param(ws.FormParameter(paramPassword, "User password").DataType("string")))

	ws.Route(ws.POST("/resource").
		Produces("text/plain").
		Consumes("application/x-www-form-urlencoded").
		To(authz).
		Param(ws.FormParameter(paramClientID, "ClientID").DataType("string")).
		Param(ws.FormParameter(paramUser, "User name").DataType("string")).
		Param(ws.FormParameter(paramName, "Topic (name or filter)").DataType("string")).
		Param(ws.FormParameter(paramPermission, "Permission type").DataType("string")))

	ws.Route(ws.POST("/tok/user").
		Produces("text/plain").
		Consumes("application/x-www-form-urlencoded").
		Filter(apiAuth).
		To(authn).
		Param(ws.FormParameter(paramClientID, "ClientID").DataType("string")).
		Param(ws.FormParameter(paramUser, "User name").DataType("string")).
		Param(ws.FormParameter(paramPassword, "User password").DataType("string")))

	ws.Route(ws.POST("/tok/resource").
		Produces("text/plain").
		Consumes("application/x-www-form-urlencoded").
		Filter(apiAuth).
		To(authz).
		Param(ws.FormParameter(paramClientID, "ClientID").DataType("string")).
		Param(ws.FormParameter(paramUser, "User name").DataType("string")).
		Param(ws.FormParameter(paramName, "Topic (name or filter)").DataType("string")).
		Param(ws.FormParameter(paramPermission, "Permission type").DataType("string")))

	wsContainer := restful.NewContainer()

	wsContainer.Add(ws)

	srv := &http.Server{
		Addr:    ":6062",
		Handler: wsContainer,
	}

	go func() {
		_ = srv.ListenAndServe()
	}()

	m.Run()

	_ = srv.Shutdown(context.Background())

	os.Exit(0)
}

func TestInvalidPath(t *testing.T) {
	f, err := newFront(config{
		ApiToken: "",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/usr",
			Resource: "http://localhost:6062/mqttauth/rsr",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("bla", "", "")
	require.Equal(t, vlauth.StatusDeny, st)

	st = f.ACL("bla", "", "", vlauth.AccessRead)
	require.Equal(t, vlauth.StatusDeny, st)
}

func TestBadUser(t *testing.T) {
	f, err := newFront(config{
		ApiToken: "",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/user",
			Resource: "http://localhost:6062/mqttauth/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("bla", "testsr", "testpassword")
	require.Equal(t, vlauth.StatusDeny, st)
}

func TestBadPassword(t *testing.T) {
	f, err := newFront(config{
		ApiToken: "",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/user",
			Resource: "http://localhost:6062/mqttauth/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("bla", "testuser", "testpasswrd")
	require.Equal(t, vlauth.StatusDeny, st)
}

func TestValidAuthn(t *testing.T) {
	f, err := newFront(config{
		ApiToken: "",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/user",
			Resource: "http://localhost:6062/mqttauth/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("testclient", "testuser", "testpassword")
	require.Equal(t, vlauth.StatusAllow, st)
}

func TestInvalidAuthz(t *testing.T) {
	f, err := newFront(config{
		ApiToken: "",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/user",
			Resource: "http://localhost:6062/mqttauth/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("testclient", "testuser", "testpassword")
	require.Equal(t, vlauth.StatusAllow, st)

	st = f.ACL("testclient", "testuser", "/topic/v2/value", vlauth.AccessRead)
	require.Equal(t, vlauth.StatusDeny, st)

	st = f.ACL("testclient", "testuser", "/topic/v1/value", vlauth.AccessWrite)
	require.Equal(t, vlauth.StatusDeny, st)
}

func TestValidAuthz(t *testing.T) {
	f, err := newFront(config{
		ApiToken: "",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/user",
			Resource: "http://localhost:6062/mqttauth/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("testclient", "testuser", "testpassword")
	require.Equal(t, vlauth.StatusAllow, st)

	st = f.ACL("testclient", "testuser", "/topic/v1/value", vlauth.AccessRead)
	require.Equal(t, vlauth.StatusAllow, st)

	st = f.ACL("testclient", "testuser", "/topic/v2/value", vlauth.AccessWrite)
	require.Equal(t, vlauth.StatusAllow, st)
}

func TestValidPathInvalidToken(t *testing.T) {
	f, err := newFront(config{
		ApiToken: verySecretToken + "1",
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/tok/user",
			Resource: "http://localhost:6062/mqttauth/tok/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("testclient", "testuser", "testpassword")
	require.Equal(t, vlauth.StatusDeny, st)

	st = f.ACL("testclient", "testuser", "/topic/v1/value", vlauth.AccessRead)
	require.Equal(t, vlauth.StatusDeny, st)

	st = f.ACL("testclient", "testuser", "/topic/v2/value", vlauth.AccessWrite)
	require.Equal(t, vlauth.StatusDeny, st)
}

func TestValidPathValidToken(t *testing.T) {
	f, err := newFront(config{
		ApiToken: verySecretToken,
		Path: configPath{
			User:     "http://localhost:6062/mqttauth/tok/user",
			Resource: "http://localhost:6062/mqttauth/tok/resource",
		},
	})

	require.NoError(t, err)
	require.NotNil(t, f)

	st := f.Password("testclient", "testuser", "testpassword")
	require.Equal(t, vlauth.StatusAllow, st)

	st = f.ACL("testclient", "testuser", "/topic/v1/value", vlauth.AccessRead)
	require.Equal(t, vlauth.StatusAllow, st)

	st = f.ACL("testclient", "testuser", "/topic/v2/value", vlauth.AccessWrite)
	require.Equal(t, vlauth.StatusAllow, st)
}
