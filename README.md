# Overview
[![pipeline status](https://gitlab.com/volantmq/vlplugin/auth/http/badges/master/pipeline.svg)](https://gitlab.com/volantmq/vlplugin/auth/http/commits/master)

This plugin provides the ability for your VolantMQ server to perform authentication (determining who can log in) and authorisation (determining what permissions they have) by making requests to an HTTP server.
This plugin can put a significant amount of load on its backing service.

## Configuring and enabling the Plugin

```yaml
plugins:
  enabled:
    - auth_http
  config:
    auth: # plugin type
      - name: http1
        backend: http
        config:
          path:
            user: http://some-server/auth/user
            resource: http://some-server/auth/resource
      - name: http2
        backend: http
        config:
          path:
            user: https://some-another-server/auth/user
            resource: https://some-another-server/auth/resource
```

### API authentication
Auth backend may want authentication to API call itself using `Authentication` header. If so environment variable `VOLANTMQ_PLUGIN_AUTH_<name>_TOKEN` can be supplied.
Lets assume from config above that backend accessed by `http2` needs token. Environment variable then looks as `VOLANTMQ_PLUGIN_AUTH_HTTP2_TOKEN=<token>`
Value in variable must not contain `Bearer ` prefix

## What Must My Web Server Do?
This plugin requires that your web server respond to requests in a certain predefined format. It will make POST requests against the URIs listed in the configuration. It will add parameters as a URL-encoded request body as follows:

### user_path
* `namespace` - the namespace server belongs to
* `clientId` - the clientId used be connection
* `user` - the name of the user (can be missing)
* `password` - the password provided (can be empty)

### resource_path
* `namespace` - the namespace server belongs to
* `clientId` - the clientId used be connection
* `user` - the name of the user (can be empty)
* `name` - topic name or filter
* `permission` - the access level to the resource (`write` or `read`)

Your web server should always return HTTP 200 OK, with a body containing:

* `deny`  - deny access to the user / resource
* `allow` - allow access to the user / resource

### Backend example
Take a look `auth_test.go` for basics in backend implementation 
