package main

import (
	"github.com/VolantMQ/vlapi/vlauth"
	"github.com/go-resty/resty/v2"
)

const (
	paramUser       = string("user")
	paramPassword   = string("password")
	paramClientID   = string("clientId")
	paramName       = string("name")
	paramPermission = string("permission")
	paramNamespace  = string("namespace")
)

type front struct {
	rest *resty.Client
	cfg  config
}

func newFront(c config) (*front, error) {
	f := &front{
		rest: resty.New(),
		cfg:  c,
	}

	return f, nil
}

func (a *front) Password(clientID, user, password string) error {
	req := a.rest.R()

	req.SetAuthToken(a.cfg.ApiToken)
	req.SetFormData(map[string]string{
		paramNamespace: a.cfg.Namespace,
		paramClientID:  clientID,
		paramUser:      user,
		paramPassword:  password,
	})

	resp, err := req.Post(a.cfg.Path.User)
	if err == nil && resp.StatusCode() == 200 && string(resp.Body()) == "allow" {
		return vlauth.StatusAllow
	}

	return vlauth.StatusDeny
}

func (a *front) ACL(clientID, user, topic string, access vlauth.AccessType) error {
	req := a.rest.R()
	req.SetAuthToken(a.cfg.ApiToken)
	req.SetFormData(map[string]string{
		paramNamespace:  a.cfg.Namespace,
		paramClientID:   clientID,
		paramUser:       user,
		paramName:       topic,
		paramPermission: access.Type(),
	})

	resp, err := req.Post(a.cfg.Path.Resource)
	if err == nil && resp.StatusCode() == 200 && string(resp.Body()) == "allow" {
		return vlauth.StatusAllow
	}

	return vlauth.StatusDeny
}

func (a *impl) Shutdown() error {
	return nil
}
