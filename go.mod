module gitlab.com/VolantMQ/vlplugin/auth/http

go 1.13

require (
	github.com/VolantMQ/vlapi v0.5.6
	github.com/emicklei/go-restful v2.11.1+incompatible
	github.com/go-resty/resty/v2 v2.2.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v3 v3.0.0-20200121175148-a6ecf24a6d71
)
