package main

import (
	"errors"
	"fmt"
	"reflect"

	"github.com/VolantMQ/vlapi/vlauth"
	"github.com/VolantMQ/vlapi/vlplugin"
	"gopkg.in/yaml.v3"
)

type pl struct {
	vlplugin.Descriptor
}

type configPath struct {
	User     string `mapstructure:"user,omitempty" yaml:"user,omitempty" json:"user,omitempty" default:""`
	Resource string `mapstructure:"resource,omitempty" yaml:"resource,omitempty" json:"resource,omitempty" default:""`
}

type config struct {
	Namespace string     `mapstructure:"namespace,omitempty" yaml:"namespace,omitempty" json:"namespace,omitempty" default:""`
	ApiToken  string     `mapstructure:"apiToken,omitempty" yaml:"apiToken,omitempty" json:"apiToken,omitempty" default:""`
	Path      configPath `mapstructure:"path,omitempty" yaml:"path,omitempty" json:"path,omitempty" default:""`
}

type impl struct {
	*vlplugin.SysParams
	*front
}

var _ vlplugin.Plugin = (*pl)(nil)
var _ vlplugin.Info = (*pl)(nil)
var _ vlauth.IFace = (*impl)(nil)

// Plugin symbol
var Plugin pl

var version string

func init() {
	Plugin.V = version
	Plugin.N = "http"
	Plugin.T = "auth"
}

func (pl *pl) Load(c interface{}, params *vlplugin.SysParams) (pla interface{}, err error) {
	p := &impl{
		SysParams: params,
	}

	var cfg config

	decodeIFace := func() error {
		var data []byte
		var e error
		if data, e = yaml.Marshal(c); e != nil {
			e = errors.New(Plugin.T + "." + Plugin.N + ": " + e.Error())
			return e
		}

		if e = yaml.Unmarshal(data, &cfg); e != nil {
			e = errors.New(Plugin.T + "." + Plugin.N + ": " + e.Error())
			return e
		}

		return e
	}

	switch t := c.(type) {
	case map[string]interface{}:
		if err = decodeIFace(); err != nil {
			return
		}
	case map[interface{}]interface{}:
		if err = decodeIFace(); err != nil {
			return
		}
	case []byte:
		if err = yaml.Unmarshal(t, &cfg); err != nil {
			err = errors.New(Plugin.T + "." + Plugin.N + ": " + err.Error())
			return
		}
	default:
		err = fmt.Errorf("%s.%s: invalid config type %s", Plugin.T, Plugin.N, reflect.TypeOf(c).String())
		return
	}

	if p.front, err = newFront(cfg); err != nil {
		return
	}

	return p, nil
}

// Info plugin info
func (pl *pl) Info() vlplugin.Info {
	return pl
}

func main() {
	panic("this is a plugin, build it as with -buildmode=plugin")
}
